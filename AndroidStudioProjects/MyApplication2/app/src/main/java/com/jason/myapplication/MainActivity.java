package com.jason.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jason.myapplication.RequestBody.LoginRequest;
import com.jason.myapplication.ResponseBody.LoginResponse;
import com.jason.myapplication.ResponseBody.WeatherResponse;
import com.jason.myapplication.ResponseBody.WeatherResponseHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Url;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;

    EditText city;
    Button login;

    String appId = "d43eb9abd2a6363c0bb6597e9e5bb991";

    ImageView bg_weather;
    TextView welcome;

    TextView langitude;
    TextView longitude;
    TextView temperature;
    TextView weatherType;
    TextView pressure;
    TextView humidity;
    TextView temp_min;
    TextView temp_max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(city.getWindowToken(), 0);

                progressBar.setVisibility(View.VISIBLE);

                if (!city.getText().toString().isEmpty()) {
                    getWeather();
                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setTitle("City is empty");
                    alertDialog.setMessage("Please enter city name");
                    alertDialog.setCancelable(true);
                    alertDialog.setPositiveButton("Ok", null);
                    alertDialog.show();
                }
            }
        });
    }

    private void getWeather() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        HttpInterface httpInterface = retrofit.create(HttpInterface.class);
        Call<WeatherResponse> call = httpInterface.getWeather(city.getText().toString(), appId);

        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200) {
//                            welcome.setVisibility(View.INVISIBLE);

                    langitude.setText(response.body().getCoord().getLat().toString());
                    longitude.setText(response.body().getCoord().getLon().toString());

                    temperature.setText(response.body().getMain().getTemp().toString());
                    temp_min.setText(response.body().getMain().getTempMin().toString());
                    temp_max.setText(response.body().getMain().getTemp().toString());
                    pressure.setText(response.body().getMain().getPressure().toString());
                    humidity.setText(response.body().getMain().getHumidity().toString());
                    weatherType.setText(response.body().getWeather().get(0).getMain());

                    welcome.setText(response.body().getName());

                    if (response.body().getWeather().get(0).getMain().equals("Clouds")) {
                        bg_weather.setImageResource(R.mipmap.cloudy);
                    } else if (response.body().getWeather().get(0).getMain().equals("Clear")) {
                        bg_weather.setImageResource(R.mipmap.clear);
                    } else if (response.body().getWeather().get(0).getMain().equals("Sun")) {
                        bg_weather.setImageResource(R.mipmap.sunny);
                    } else if (response.body().getWeather().get(0).getMain().equals("Rain")) {
                        bg_weather.setImageResource(R.mipmap.rainy);
                    } else if (response.body().getWeather().get(0).getMain().equals("Haze")) {
                        bg_weather.setImageResource(R.mipmap.haze);
                    } else if (response.body().getWeather().get(0).getMain().equals("Thunder")) {
                        bg_weather.setImageResource(R.mipmap.thnder_storm);
                    } else {
                        bg_weather.setImageResource(R.mipmap.clear);
                    }
                } else {
                    langitude.setText("-");
                    longitude.setText("-");

                    temperature.setText("-");
                    temp_min.setText("-");
                    temp_max.setText("-");
                    pressure.setText("-");
                    humidity.setText("-");
                    weatherType.setText("-");

                    welcome.setText("City cannot be found");
                    bg_weather.setImageResource(R.mipmap.clear);
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.getMessage();
            }
        });
    }

    private void initView() {
        progressBar = findViewById(R.id.progressBar1);
        bg_weather = findViewById(R.id.bg_weather);
        welcome = findViewById(R.id.welcome);

        city = findViewById(R.id.city);
        login = findViewById(R.id.login);
        langitude = findViewById(R.id.langResult);
        longitude = findViewById(R.id.longResult);
        pressure = findViewById(R.id.pressResult);
        humidity = findViewById(R.id.humidResult);
        temp_min = findViewById(R.id.minTempResult);
        temp_max = findViewById(R.id.maxTempResult);
        temperature = findViewById(R.id.temperature);
        weatherType = findViewById(R.id.weather_type);
    }

//    private class ReadUrl extends AsyncTask<String, String, String> {
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressBar.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            HttpURLConnection httpURLConnection = null;
//            BufferedReader bufferedReader = null;
//            try{
//                URL url = new URL(strings[0]);
//                httpURLConnection = (HttpURLConnection) url.openConnection();
//                httpURLConnection.connect();
//
//                InputStream inputStream = httpURLConnection.getInputStream();
//
//                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//
//                StringBuffer stringBuffer = new StringBuffer();
//                String line = "";
//
//                while ((line = bufferedReader.readLine()) != null) {
//                    stringBuffer.append(line+"\n");
//                    Log.e("Processed Response = ", line);
//                }
//                return stringBuffer.toString();
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if (httpURLConnection != null) {
//                    httpURLConnection.disconnect();
//                }
//                try {
//                    if (bufferedReader != null) {
//                        bufferedReader.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            if (progressBar.getVisibility() == View.VISIBLE){
//                progressBar.setVisibility(View.GONE);
//            }
//        }
//    }
}
