package com.jason.myapplication;

import com.jason.myapplication.RequestBody.LoginRequest;
import com.jason.myapplication.ResponseBody.LoginResponse;
import com.jason.myapplication.ResponseBody.WeatherResponse;
import com.jason.myapplication.ResponseBody.WeatherResponseHandler;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HttpInterface {
    @POST("/rest/v2/all")
    Call<LoginResponse> initLogin(@Body LoginRequest loginRequest);

    @GET("/rest/v2/all")
    Call<LoginResponse> getCountries();

    @POST("/data/2.5/weather")
    Call<WeatherResponse> getWeather(@Query("q") String city, @Query("appid") String appid);
}
